-- phpMyAdmin SQL Dump
-- version 4.6.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2016-07-20 12:57:21
-- 服务器版本： 5.6.29-log
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dingtalk`
--

-- --------------------------------------------------------

--
-- 表的结构 `dt_appointment`
--

CREATE TABLE `dt_appointment` (
  `id` int(11) NOT NULL,
  `unionid` varchar(100) NOT NULL COMMENT 'unionid',
  `boardroom_id` int(10) NOT NULL COMMENT '会议室id',
  `date` varchar(100) NOT NULL COMMENT '日期',
  `start_time` varchar(100) NOT NULL COMMENT '开始时间',
  `end_time` varchar(100) NOT NULL COMMENT '结束时间',
  `title` varchar(100) NOT NULL COMMENT '议题',
  `allow` int(10) NOT NULL DEFAULT '0' COMMENT '是否允许',
  `allownote` text COMMENT '原因',
  `addtime` varchar(100) NOT NULL COMMENT '添加时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dt_appointment`
--

INSERT INTO `dt_appointment` (`id`, `unionid`, `boardroom_id`, `date`, `start_time`, `end_time`, `title`, `allow`, `allownote`, `addtime`) VALUES
(1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 1, '1465920000', '1465994940', '1466005740', '看看', 0, NULL, '1465994979'),
(2, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 1, '1465920000', '1465994940', '1466005740', '看看', 0, NULL, '1465994991'),
(3, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 1, '1465920000', '1465994940', '1466005740', '看看', 0, NULL, '1465994995'),
(4, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 1, '1465920000', '1465994940', '1466005740', '看看', 0, NULL, '1465994999'),
(5, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 1, '1465920000', '1465995000', '1465995000', '看看', 0, NULL, '1465995011'),
(6, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 1, '1465920000', '1465995000', '1465995000', '看看', 0, NULL, '1465995022'),
(7, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 1, '1465920000', '1465995000', '1465995000', '看看', 0, NULL, '1465995038'),
(8, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 1, '1465920000', '1465959000', '1466005800', '哦哦', 1, '吧', '1465995054'),
(9, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 1, '1466697600', '1466770380', '1466770380', '你好，！！', 1, '1', '1466770444'),
(10, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 1, '', '', '', '', 0, NULL, '1466787750'),
(11, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 1, '', '', '', '', 0, NULL, '1466787997'),
(12, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 1, '1466784000', '1466799780', '1466814180', '看看', 1, '看看', '1466789012');

-- --------------------------------------------------------

--
-- 表的结构 `dt_approve`
--

CREATE TABLE `dt_approve` (
  `id` int(11) NOT NULL,
  `unionid` varchar(50) NOT NULL COMMENT 'unionid',
  `approve_class` int(10) NOT NULL COMMENT '审批类型',
  `approve_id` int(10) NOT NULL COMMENT '审批id',
  `userid` varchar(50) NOT NULL COMMENT '审批人userid',
  `addtime` varchar(50) NOT NULL COMMENT '添加时间',
  `allow` int(10) NOT NULL DEFAULT '0' COMMENT '是否审批',
  `allower` varchar(50) DEFAULT NULL COMMENT '审批人',
  `allownote` text COMMENT '审批备注',
  `allowtime` varchar(50) DEFAULT NULL COMMENT '审批时间',
  `remove` int(10) NOT NULL DEFAULT '0' COMMENT '是否销假',
  `removenote` text,
  `removetime` varchar(50) DEFAULT NULL COMMENT '销假时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dt_approve`
--

INSERT INTO `dt_approve` (`id`, `unionid`, `approve_class`, `approve_id`, `userid`, `addtime`, `allow`, `allower`, `allownote`, `allowtime`, `remove`, `removenote`, `removetime`) VALUES
(1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, 1, 'manager4617', '1465834688', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '看见了', NULL, 0, NULL, NULL),
(2, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 1, 1, 'manager4617', '1465836243', 2, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '看看', NULL, 0, NULL, NULL),
(3, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 2, 1, 'manager4617', '1465834669', 0, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '不', NULL, 0, NULL, NULL),
(4, 'mlpPWiS8ZnzY6o2BPwR32EgiEiE', 2, 2, '032862441032987435', '1465633894', 0, NULL, NULL, NULL, 0, NULL, NULL),
(5, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, 2, 'manager4617', '1466769512', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '看看', NULL, 0, NULL, NULL),
(6, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, 3, 'manager4617', '1466770546', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '同意', NULL, 0, NULL, NULL),
(7, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, 4, 'manager4617', '1466787633', 0, NULL, NULL, NULL, 0, NULL, NULL),
(8, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, 5, 'manager4617', '1466788862', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '看看', NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `dt_boardroom`
--

CREATE TABLE `dt_boardroom` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL COMMENT '名称',
  `wifi` int(10) NOT NULL COMMENT 'wifi',
  `projection` int(10) NOT NULL COMMENT '投影',
  `num` int(100) NOT NULL COMMENT '最大人数',
  `floor` varchar(20) NOT NULL COMMENT '楼层',
  `address` varchar(100) NOT NULL COMMENT '地点',
  `comment` text NOT NULL COMMENT '备注',
  `addtime` varchar(100) NOT NULL COMMENT '添加时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dt_boardroom`
--

INSERT INTO `dt_boardroom` (`id`, `name`, `wifi`, `projection`, `num`, `floor`, `address`, `comment`, `addtime`) VALUES
(1, '多媒体会议室会议室', 1, 1, 100, '一楼', '广西水利电力职业技术学院', '没有空调', '1465922133');

-- --------------------------------------------------------

--
-- 表的结构 `dt_business`
--

CREATE TABLE `dt_business` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL COMMENT '姓名',
  `address` varchar(100) NOT NULL,
  `start` varchar(50) NOT NULL COMMENT '开始时间',
  `end` varchar(50) NOT NULL COMMENT '结束时间',
  `how` int(50) NOT NULL COMMENT '请假天数',
  `why` text NOT NULL COMMENT '请假事由',
  `pic` varchar(100) DEFAULT NULL COMMENT '图片'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dt_business`
--

INSERT INTO `dt_business` (`id`, `name`, `address`, `start`, `end`, `how`, `why`, `pic`) VALUES
(1, '麦青强', '北京', '1465488000', '1465488000', 1, '55', '/dingtalk/Uploads/20160610/575a94873c88a.jpg');

-- --------------------------------------------------------

--
-- 表的结构 `dt_leave`
--

CREATE TABLE `dt_leave` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL COMMENT '姓名',
  `class` int(10) NOT NULL COMMENT '请假类型',
  `start` varchar(50) NOT NULL COMMENT '开始时间',
  `end` varchar(50) NOT NULL COMMENT '结束时间',
  `how` int(50) NOT NULL COMMENT '请假天数',
  `why` text NOT NULL COMMENT '请假事由',
  `pic` varchar(100) DEFAULT NULL COMMENT '图片'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dt_leave`
--

INSERT INTO `dt_leave` (`id`, `name`, `class`, `start`, `end`, `how`, `why`, `pic`) VALUES
(1, '麦青强', 1, '1465510740', '1465510740', 3, '555', '/dingtalk/Uploads/20160610/575a945e6e9a3.jpg'),
(2, '麦青强', 1, '1466726220', '1466726220', 3, '看看', '/dingtalk/Uploads/20160624/576d205120083.png'),
(3, '麦青强', 1, '1466727240', '1466727240', 2, '看看', '/dingtalk/Uploads/20160624/576d245cc20e7.jpg'),
(4, '麦青强', 1, '', '', 0, '', '/dingtalk/Uploads/'),
(5, '麦青强', 1, '1466788800', '1466788800', 1, '适配器', '/dingtalk/Uploads/20160625/576d6bdcb1838.jpg');

-- --------------------------------------------------------

--
-- 表的结构 `dt_leave_class`
--

CREATE TABLE `dt_leave_class` (
  `id` int(11) NOT NULL,
  `classid` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dt_leave_class`
--

INSERT INTO `dt_leave_class` (`id`, `classid`, `name`) VALUES
(1, '1', '事假'),
(2, '2', '病假'),
(3, '3', '年假'),
(4, '4', '调休'),
(5, '5', '婚假'),
(6, '6', '产假'),
(7, '7', '陪产假'),
(8, '8', '路途假'),
(9, '0', '其他');

-- --------------------------------------------------------

--
-- 表的结构 `dt_log`
--

CREATE TABLE `dt_log` (
  `id` int(11) NOT NULL,
  `title` varchar(120) NOT NULL,
  `content` text NOT NULL,
  `log_class` int(2) NOT NULL,
  `unionid` varchar(100) NOT NULL,
  `share` int(2) NOT NULL,
  `time` varchar(100) NOT NULL,
  `addtime` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dt_log`
--

INSERT INTO `dt_log` (`id`, `title`, `content`, `log_class`, `unionid`, `share`, `time`, `addtime`) VALUES
(3, '今晚通宵完成钉钉', '不睡觉', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '1466699400', '1466699423'),
(4, '录制视频', '录制视频', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '1466713680', '1466756919'),
(5, '22', '22', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '1466713920', '1466757178'),
(6, '222', '啊啊啊', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '1466713980', '1466757197'),
(7, '今晚通宵完成钉钉', '吧', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '1466713980', '1466757215'),
(8, '222', '图太', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '1466713980', '1466757235'),
(9, '222', '图太', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '1466713980', '1466757313'),
(10, '222', '图太', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '1466713980', '1466759317'),
(11, '55', '现在', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '1466716140', '1466759346'),
(12, '看见了', '看见了', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '1466716200', '1466759427'),
(13, '看见了', '太坑了', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '1466716260', '1466759489'),
(14, '看见了', '太坑了', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '1466716260', '1466759508'),
(15, '看见了', '太坑了', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '1466716260', '1466759560'),
(16, '', '', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '', '1466759630'),
(17, '', '', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '', '1466759783'),
(18, '', '', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '', '1466759882'),
(19, '', '', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '', '1466759975'),
(20, '', '', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '', '1466759991'),
(21, '', '', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '', '1466760022'),
(22, '', '', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '', '1466760057'),
(23, '', '', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '', '1466760063'),
(24, '', '', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '', '1466760077'),
(25, '', '', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '', '1466760119'),
(26, '', '', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '', '1466760192'),
(27, '', '', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '', '1466760207'),
(28, '', '', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '', '1466760258'),
(29, '', '', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '', '1466760285'),
(30, '', '', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '', '1466760363'),
(31, '', '', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '', '1466760369'),
(32, '', '', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '', '1466760765'),
(33, '', '', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '', '1466760844'),
(34, '', '', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '', '1466760952'),
(35, '', '', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '', '1466760970'),
(36, '你好，！！', '你好', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '1466727360', '1466770608'),
(37, '', '', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '', '1466788021'),
(38, '啊啊啊', '看见了', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '1466788080', '1466788129'),
(39, '哦哦哦', '累了困了', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '1466788140', '1466788151'),
(40, '累了困了', '涂涂乐', 2, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '1466788140', '1466788167'),
(41, '看见了', '考虑提供', 2, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '1466788140', '1466788185'),
(42, '刚刚', '看见了', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, '1466789040', '1466789131');

-- --------------------------------------------------------

--
-- 表的结构 `dt_notice`
--

CREATE TABLE `dt_notice` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL COMMENT '公告标题',
  `content` text NOT NULL COMMENT '公告内容',
  `unionid` varchar(100) NOT NULL COMMENT '发布人唯一标识',
  `username` varchar(50) NOT NULL COMMENT '发布人',
  `time` int(50) NOT NULL COMMENT '发布时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dt_notice`
--

INSERT INTO `dt_notice` (`id`, `title`, `content`, `unionid`, `username`, `time`) VALUES
(1, '明天放假', '明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假', '123456789', '', 1464437916),
(2, '年底分红通知', '年底分红通知年底分红通知年底分红通知年底分红通知年底分红通知年底分红通知', '123456788', '', 1464427916),
(3, '明天放假', '明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假', '123456789', '', 1464437916),
(4, '年底分红通知', '年底分红通知年底分红通知年底分红通知年底分红通知年底分红通知年底分红通知', '123456788', '', 1464427916),
(5, '明天放假', '明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假', '123456789', '', 1464437916),
(6, '年底分红通知', '年底分红通知年底分红通知年底分红通知年底分红通知年底分红通知年底分红通知', '123456788', '', 1464427916),
(7, '明天放假', '明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假', '123456789', '', 1464437916),
(8, '年底分红通知', '年底分红通知年底分红通知年底分红通知年底分红通知年底分红通知年底分红通知', '123456788', '', 1464427916),
(9, '看看', '看看', 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', 1466769096),
(10, '看看', '看看', 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', 1466769357),
(11, '你好', '你好', 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', 1466770016),
(12, '你好，！！', '你好', 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', 1466770316),
(13, '2', '21', 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', 1466787529),
(14, '22222', '', 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', 1466787592),
(15, '看看', '看你', 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', 1466788664);

-- --------------------------------------------------------

--
-- 表的结构 `dt_read_notice`
--

CREATE TABLE `dt_read_notice` (
  `id` int(11) NOT NULL,
  `unionid` varchar(50) NOT NULL,
  `notice_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `time` int(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dt_read_notice`
--

INSERT INTO `dt_read_notice` (`id`, `unionid`, `notice_id`, `name`, `time`) VALUES
(1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 1, '麦青强', 1466044919),
(2, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 5, '麦青强', 1466044931),
(3, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 7, '麦青强', 1466228346),
(4, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 10, '麦青强', 1466769366),
(5, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 11, '麦青强', 1466770023),
(6, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 12, '麦青强', 1466770324),
(7, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 15, '麦青强', 1466788674),
(8, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 14, '麦青强', 1467349227);

-- --------------------------------------------------------

--
-- 表的结构 `dt_reimburse`
--

CREATE TABLE `dt_reimburse` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL COMMENT '姓名',
  `money` varchar(50) NOT NULL,
  `classname` varchar(100) NOT NULL,
  `details` text NOT NULL,
  `pic` varchar(100) DEFAULT NULL COMMENT '图片'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dt_reimburse`
--

INSERT INTO `dt_reimburse` (`id`, `name`, `money`, `classname`, `details`, `pic`) VALUES
(1, '麦青强', '225.55', '图标', '看看', '/dingtalk/Uploads/20160610/575a94b9d5207.jpg'),
(2, '苗志锋', '100', '出差', '差旅100', '/dingtalk/Uploads/');

-- --------------------------------------------------------

--
-- 表的结构 `dt_report`
--

CREATE TABLE `dt_report` (
  `id` int(11) NOT NULL,
  `content` text NOT NULL,
  `unionid` varchar(100) NOT NULL,
  `report_class` int(2) NOT NULL,
  `time_id` int(11) NOT NULL,
  `addtime` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dt_report`
--

INSERT INTO `dt_report` (`id`, `content`, `unionid`, `report_class`, `time_id`, `addtime`) VALUES
(1, '看看', 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 1, 1, '1466766678'),
(2, '', 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, 0, '1466770649');

-- --------------------------------------------------------

--
-- 表的结构 `dt_report_day`
--

CREATE TABLE `dt_report_day` (
  `id` int(11) NOT NULL,
  `date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dt_report_day`
--

INSERT INTO `dt_report_day` (`id`, `date`) VALUES
(1, '2016-06-24');

-- --------------------------------------------------------

--
-- 表的结构 `dt_report_month`
--

CREATE TABLE `dt_report_month` (
  `id` int(11) NOT NULL,
  `year` int(5) NOT NULL,
  `month` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `dt_report_week`
--

CREATE TABLE `dt_report_week` (
  `id` int(11) NOT NULL,
  `year` int(10) NOT NULL,
  `month` int(5) NOT NULL,
  `week` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `dt_sign`
--

CREATE TABLE `dt_sign` (
  `id` int(11) NOT NULL,
  `unionid` varchar(100) NOT NULL COMMENT 'unionid',
  `name` varchar(100) NOT NULL COMMENT '姓名',
  `address` varchar(100) NOT NULL COMMENT '地址',
  `latitude` varchar(50) NOT NULL COMMENT '纬度',
  `longitude` varchar(50) NOT NULL COMMENT '经度',
  `time` varchar(50) NOT NULL COMMENT '签到时间',
  `note` text COMMENT '备注',
  `pic` varchar(100) DEFAULT NULL COMMENT '图片'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='签到';

--
-- 转存表中的数据 `dt_sign`
--

INSERT INTO `dt_sign` (`id`, `unionid`, `name`, `address`, `latitude`, `longitude`, `time`, `note`, `pic`) VALUES
(7, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465489653', 'tyhr', '/dingtalk/Uploads/20160610/575998ff0109e.jpg'),
(8, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465489664', 'dfsg', '/dingtalk/Uploads/20160610/5759999d4da05.jpg'),
(9, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465489823', 'sdaf', '/dingtalk/Uploads/20160610/57599a5fe6bb8.jpg'),
(10, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465490018', 'ds ', ''),
(11, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465490101', '545', ''),
(12, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465490150', '', ''),
(13, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465490296', '', ''),
(14, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465490365', '565', '/Uploads/20160610/57599bcc5066e.jpg'),
(15, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465490381', '', ''),
(16, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465490644', '', '20160610/57599d3e7649e.jpg'),
(17, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465490752', '', '/dingtalk/Upload/20160610/57599e5d6e446.jpg'),
(18, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465491074', '', '/dingtalk/Upload/20160610/57599e8c1760a.jpg'),
(19, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207882', '108.184408', '1465491181', '', '/dingtalk/Uploads/20160610/57599ef648f8c.jpg'),
(20, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465536700', '', '/dingtalk/Uploads/20160610/575a52ef53d4b.jpg'),
(21, 'mlpPWiS8ZnzY6o2BPwR32EgiEiE', '苗志锋', '广西壮族自治区南宁市兴宁区长堽路三里二里二巷靠近广西师范学院-田径足球运动场', '22.836381', '108.351102', '1465633767', '', '/dingtalk/Uploads/'),
(22, 'S519Syc8yvnvaWrsCmiSjiSAiEiE', '黄家桂', '广西壮族自治区南宁市武鸣区教育路靠近跨时代美食城', '23.207906', '108.183928', '1465898736', '', '/dingtalk/Uploads/'),
(23, 'S519Syc8yvnvaWrsCmiSjiSAiEiE', '黄家桂', '广西壮族自治区南宁市武鸣区教育路靠近跨时代美食城', '23.207906', '108.183928', '1465898736', '', '/dingtalk/Uploads/'),
(24, 'yFctqE0YOmFfDGiiiPfiifhKwiEiE', 'Hct', '', '', '', '1465998283', '', '/dingtalk/Uploads/'),
(25, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207873', '108.184404', '1466044749', '我在这里', ''),
(26, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西水利电力职业技术学院(北门)', '23.207833', '108.183253', '1466044959', '我在北门', '/dingtalk/Uploads/20160616/5762125076e67.jpg'),
(27, 'mlpPWiS8ZnzY6o2BPwR32EgiEiE', '苗志锋', '广西壮族自治区南宁市兴宁区长堽路三里二里二巷靠近广西师范学院-田径足球运动场', '22.83625', '108.351169', '1466420956', '', ''),
(28, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207859', '108.184394', '1466769390', '看看', '/dingtalk/Uploads/20160624/576d200d5f2bd.png'),
(29, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近鑫鲜火锅城', '23.207871', '108.184388', '1466770053', '看看', '/dingtalk/Uploads/20160624/576d22b42ec0d.png'),
(30, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207844', '108.184406', '1466770347', '看看', '/dingtalk/Uploads/20160624/576d23c24e3e1.jpg'),
(31, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207874', '108.184398', '1466786008', '', ''),
(32, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207874', '108.184398', '1466786207', '', ''),
(33, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207874', '108.184398', '1466786227', '', ''),
(34, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207874', '108.184398', '1466786233', '', ''),
(35, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207874', '108.184398', '1466786295', '', ''),
(36, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207874', '108.184398', '1466787603', '', ''),
(37, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207852', '108.184401', '1466788705', '看看', '/dingtalk/Uploads/20160625/576d6b8350d0f.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dt_appointment`
--
ALTER TABLE `dt_appointment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dt_approve`
--
ALTER TABLE `dt_approve`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dt_boardroom`
--
ALTER TABLE `dt_boardroom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dt_business`
--
ALTER TABLE `dt_business`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dt_leave`
--
ALTER TABLE `dt_leave`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dt_leave_class`
--
ALTER TABLE `dt_leave_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dt_log`
--
ALTER TABLE `dt_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dt_notice`
--
ALTER TABLE `dt_notice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dt_read_notice`
--
ALTER TABLE `dt_read_notice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dt_reimburse`
--
ALTER TABLE `dt_reimburse`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dt_report`
--
ALTER TABLE `dt_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dt_report_day`
--
ALTER TABLE `dt_report_day`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dt_report_month`
--
ALTER TABLE `dt_report_month`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dt_report_week`
--
ALTER TABLE `dt_report_week`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dt_sign`
--
ALTER TABLE `dt_sign`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `dt_appointment`
--
ALTER TABLE `dt_appointment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- 使用表AUTO_INCREMENT `dt_approve`
--
ALTER TABLE `dt_approve`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- 使用表AUTO_INCREMENT `dt_boardroom`
--
ALTER TABLE `dt_boardroom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `dt_business`
--
ALTER TABLE `dt_business`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `dt_leave`
--
ALTER TABLE `dt_leave`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- 使用表AUTO_INCREMENT `dt_leave_class`
--
ALTER TABLE `dt_leave_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- 使用表AUTO_INCREMENT `dt_log`
--
ALTER TABLE `dt_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- 使用表AUTO_INCREMENT `dt_notice`
--
ALTER TABLE `dt_notice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- 使用表AUTO_INCREMENT `dt_read_notice`
--
ALTER TABLE `dt_read_notice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- 使用表AUTO_INCREMENT `dt_reimburse`
--
ALTER TABLE `dt_reimburse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- 使用表AUTO_INCREMENT `dt_report`
--
ALTER TABLE `dt_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- 使用表AUTO_INCREMENT `dt_report_day`
--
ALTER TABLE `dt_report_day`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `dt_report_month`
--
ALTER TABLE `dt_report_month`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dt_report_week`
--
ALTER TABLE `dt_report_week`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dt_sign`
--
ALTER TABLE `dt_sign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
