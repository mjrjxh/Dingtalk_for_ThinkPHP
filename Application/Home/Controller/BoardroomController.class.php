<?php
/**
 * Created by PhpStorm.
 * User: Mak
 * Date: 2016/5/29 0029
 * Time: 19:01
 */

namespace Home\Controller;

/**
 * 预约会议室
 * Class BoardroomController
 * @package Home\Controller
 */
class BoardroomController extends ComController
{
    /**
     * 预约首页
     */
    public function index()
    {
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);

        $Boardroom = M('boardroom');
        $ret = $Boardroom->order('addtime desc')->select();
        $this->assign("list", $ret);
//        dump($ret);
        $this->display();
    }

    /**
     * 会议室信息
     */
    public function details()
    {
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);

        $id = I("get.id");
        $Boardroom = M('boardroom');
        $ret = $Boardroom->where("id={$id}")->order('addtime desc')->find();
        $this->assign("ret", $ret);

        $Appointment=M("appointment");
        $list=$Appointment->order("addtime desc")->select();
        $this->assign("list", $list);
//        dump($ret);
        $this->display();
    }

    /**
     * 预约列表
     */
    public function appointment()
    {
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);

        $id=I("get.id");
        $Appointment=M("appointment");
        if (!empty($id)){
            $ret=$Appointment->order("addtime desc")->select();
            $this->assign("list",$ret)->assign("time",time());
        }else{
            $ret=$Appointment->order("addtime desc")->select();
            $this->assign("list",$ret)->assign("time",time());
        }
//        dump($ret);
        $this->display();
    }

    /**
     * 已审批
     */
    public function alreadyApprove()
    {
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);

        $Appointment=M("appointment");
        $where['allow'] = array('neq', 0);
        $ret = $Appointment->where($where)->order('addtime desc')->select();
        $this->assign("list", $ret);
//        dump($ret);
        $this->display();
    }

    /**
     * 未审批
     */
    public function notApprove()
    {
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);

        $Appointment=M("appointment");
        $ret = $Appointment->where("allow=0")->order('addtime desc')->select();
        $this->assign("list", $ret);
//        dump($ret);
        $this->display();
    }

    /**
     * 添加预约
     */
    public function addAppointment(){
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);

        $unionid=session("unionid");
        if(IS_POST){
            $data['unionid']=$unionid;
            $data['boardroom_id']=I("post.id");
            $data['date']=strtotime(I("post.date"));
            $data['start_time']=strtotime(I("post.start_time"));
            $data['end_time']=strtotime(I("post.end_time"));
            $data['title']=I("post.title");
            $data['addtime']=time();
            $Appointment=M("Appointment");
            if ($Appointment->add($data)){
                $this->success("预约成功!",U("Home/Boardroom/appointment")."?dd_nav_bgcolor=FF5E97F6");
            }else{
                $this->error("预约失败!");
            }
        }else{
            $this->display();
        }
    }

    public function approve(){
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);

        if (IS_POST){
            $id=I("post.id");
            $data['allow']=I("post.allow");
            $data['allownote']=I("post.allownote");
            $Appointment=M("Appointment");
            if($Appointment->where("id=$id")->save($data)){
                $this->success("提交成功",U("Home/Boardroom/alreadyApprove")."?dd_nav_bgcolor=FF5E97F6");
            }else{
                $this->error("提交失败");
            }
        }else{
            $this->display();
        }
    }
}