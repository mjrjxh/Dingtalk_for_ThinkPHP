<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags-->
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- Your app title -->
    <title>公告</title>
    <!-- Path to Framework7 Library CSS, iOS Theme -->
    <link rel="stylesheet" href="/dingtalk/Public/Home/Framework7/css/framework7.ios.min.css">
    <!-- Path to Framework7 color related styles, iOS Theme -->
    <link rel="stylesheet" href="/dingtalk/Public/Home/Framework7/css/framework7.ios.colors.min.css">
    <!-- Path to your custom app styles-->
    <link rel="stylesheet" href="<?php echo C('iconfont');?>">
    <link rel="stylesheet" href="/dingtalk/Public/Home/Framework7/css/my-app.css">
    
    <style>
        .msg {
            padding-top: 36px;
            text-align: center;
        }

        .icon-chenggong {
            color: green;
            font-size: 80px;

        }

        i {

        }
    </style>

</head>
<body>



<!-- Status bar overlay for full screen mode (PhoneGap) -->
<div class="statusbar-overlay"></div>
<!-- Views -->
<div class="views">
    <!-- Your main view, should have "view-main" class -->
    <div class="view view-main">


        <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes-->
        <div class="pages navbar-through toolbar-through">
            <!-- Page, "data-page" contains page name -->
            <div class="page">
                <!-- Scrollable page content -->
                <div class="page-content">
                    

    <div class="msg">
        <div class="msg_main">
            <?php if(isset($message)) { ?>
            <div class="icon_area"><i class="iconfont icon-chenggong"></i></div>
            <div class="text_area">
                <h2 class="msg_title">
                    <?php echo($message); ?>
                    </p></h2>
                <?php }else{?>
                <div class="icon_area"><i class="iconfont icon-chenggong"></i></div>
                <div class="text_area">
                    <h2 class="msg_title">
                        <?php echo($error); ?>
                        </p></p></h2>
                    <?php } ?>
                    <p class="msg_desc">页面自动 <a id="href" href="<?php echo($jumpUrl); ?>">跳转</a> 等待时间： <b id="wait">
                        <?php echo($waitSecond); ?>
                    </b></p>
                    </p>
                </div>
            </div>
        </div>
    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<!-- Path to Framework7 Library JS-->
<script type="text/javascript" src="/dingtalk/Public/Home/Framework7/js/framework7.min.js"></script>
<!-- Path to your app js-->
<script type="text/javascript" src="/dingtalk/Public/Home/Framework7/js/my-app.js"></script>
<!--[if (gte IE 9)|!(IE)]><!-->
<script src="http://g.alicdn.com/ilw/cdnjs/jquery/2.1.4/jquery.min.js"></script>
<!--<![endif]-->
<!--[if lte IE 8 ]>
<script src="http://g.alicdn.com/ilw/cdnjs/jquery/1.8.3/jquery.min.js"></script>
<![endif]-->

    <script type="text/javascript">
        (function () {
            var wait = document.getElementById('wait'), href = document.getElementById('href').href;
            var interval = setInterval(function () {
                var time = --wait.innerHTML;
                if (time <= 0) {
                    location.href = href;
                    clearInterval(interval);
                }
                ;
            }, 1000);
        })();
    </script>

</body>
</html>